#include "../include/connect_user.h"

void connect_user(User *user) {
    printf("[User %d has connected to server, waiting for messages]\n", user->id);
    setbuf(stdout, NULL);

    while (1) {
        char* input = malloc(MAXLINE * sizeof(char));
        ssize_t read_size = read(user->socket, input, MAXLINE * sizeof(char));
        printf("Received: %s", input);

        char* end_input;
        char* command_line = strtok_r(input, "\n", &end_input);

        while (command_line != NULL) {
            char command_line_copy[MAXLINE];
            char message_line_copy[MAXLINE];
            strcpy(message_line_copy, command_line);

            char* end_message;
            char* message_line = strtok_r(message_line_copy, ":", &end_message);
            char* message = strtok_r(NULL, ":", &end_message);

            char *command;
            char* end_command;

            strcpy(command_line_copy, message_line);
            command = strtok_r(command_line_copy, " ", &end_command);
            command = uppercase(command);

            char *args[10];
            int i;
            for (i = 0; i < 10; i++) {
                args[i] = NULL;
            }

            int index = 0;
            char* arg = strtok_r(NULL, " ", &end_command);
            while (arg != NULL) {
                args[index] = malloc(30 * sizeof(char));
                strcpy(args[index], arg);
                arg = strtok_r(NULL, " ", &end_command);
                index++;
            }

            handle_command(user, command, args, message);
            command_line = strtok_r(NULL, "\n", &end_input);
        }

        if (read_size <= 0) {
            break;
        }
    }
}

void handle_command(User* user, char* command, char** args, char* message) {
    printf("[User %s sent command \"%s\"]\n\n", user->name, command);

    if (command != NULL) {
        command = strtok(command, "\r\n");
    }

    if (strcmp(command, NICK) == 0) {
        receive_nick(user, args);
    }
    else if (strcmp(command, USER) == 0) {
        receive_user(user, args, message);
    }
    else if (strcmp(command, LIST) == 0) {
        receive_list(user, args);
    }
    else if (strcmp(command, JOIN) == 0) {
        receive_join(user, args);
    }
    else if (strcmp(command, PART) == 0) {
        receive_part(user, args, message);
    }
    else if (strcmp(command, TOPIC) == 0) {
        receive_topic(user, args, message);
    }
    else if (strcmp(command, NAMES) == 0) {
        receive_names(user, args);
    }
    else if (strcmp(command, INVITE) == 0) {
        receive_invite(user, args);
    }
    else if (strcmp(command, KICK) == 0) {
        receive_kick(user, args, message);
    }
    else if (strcmp(command, NOTICE) == 0) {
        receive_notice(user, args, message);
    }
    else if (strcmp(command, MOTD) == 0) {
        receive_motd(user);
    }
    else if (strcmp(command, TIME) == 0) {
        receive_time(user);
    }
    else if (strcmp(command, PONG) == 0) {
        receive_pong(user);
    }
    else if (strcmp(command, AWAY) == 0) {
        receive_away(user, message);
    }
    else if (strcmp(command, USERS) == 0) {
        receive_users(user);
    }
    else if (strcmp(command, ISON) == 0) {
        receive_ison(user, args);
    }
    else if (strcmp(command, PRIVMSG) == 0) {
        receive_privmsg(user, args, message);
    }
    else if (strcmp(command, PING) == 0) {
        receive_ping(user);
    }
    else if (strcmp(command, WHO) == 0) {
        receive_who(user, args);
    }
    else if (strcmp(command, WHOIS) == 0) {
        receive_whois(user, args);
    }
    else if (strcmp(command, MODE) == 0) {
        receive_mode(user, args);
    }
    else if (strcmp(command, QUIT) == 0) {
        receive_quit(user, message);
    } else {
        receive_unknown(user, command);
    }
}
