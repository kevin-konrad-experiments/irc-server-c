#include "../include/user.h"
#include "../include/util.h"
#include "../include/channel.h"
#include "../include/connect_user.h"

ListUsers* new_list_users() {
    ListUsers* list = malloc(sizeof(ListUsers));
    list->size = 0;
    list->first = NULL;
    list->last = NULL;
    return list;
}

NodeUser* new_node_user(User* user) {
    NodeUser* node = malloc(sizeof(NodeUser));
    node->user = user;
    node->next = NULL;
    return node;
}

User* new_user(char* name, char* real_name, char* hostname, int mode, int socket) {
    User* user = malloc(sizeof(User));
    user->id = users->size;
    user->name = name;
    user->real_name = "";
    user->away_msg = NULL;
    user->hostname = hostname;
    user->mode = mode;
    user->socket = socket;

    return user;
}

char* user_get_channels_names(User* user) {
    char* channel_names = strset("");
    NodeChannel* node = channels->first;

    while (node != NULL) {
        if (channel_contains_user(user, node->channel) != -1) {
            channel_names = stradd(channel_names, node->channel->name);

            if (node->next != NULL) {
                channel_names = stradd(channel_names, " ");
            }
        }

        node = node->next;
    }

    return channel_names;
}

void add_user(ListUsers* list, User* user) {
    NodeUser* node = new_node_user(user);

    if (list->last == NULL) {
        list->first = node;
    } else {
        list->last->next = node;
    }

    list->last = node;
    list->size += 1;
}

void remove_user(ListUsers* list, User* user) {
    NodeUser* previous_node_user = NULL;
    NodeUser* node_user = list->first;

    while(node_user != NULL) {
        if (node_user->user->id == user->id) {
            NodeUser* tmp = node_user;

            if (previous_node_user == NULL) {
                list->first = node_user->next;
            } else {
                previous_node_user->next = node_user->next;
            }

            if (node_user == list->last) {
                list->last = previous_node_user;
            }

            list->size -= 1;
            node_user = node_user->next;
            free(tmp);
        } else {
            previous_node_user = node_user;
            node_user = node_user->next;
        }
    }
}

char* get_user_description(User* user) {
    char* description = strset(user->name);
    description = stradd(description, "!");
    description = stradd(description, user->name);
    description = stradd(description, "@");
    description = stradd(description, user->hostname);
    return description;
}

User* get_user_by_name(char* name) {
    if (users->size == 0) {
        return NULL;
    }

    NodeUser* node = users->first;

    while (node != NULL) {
        if (strcmp(node->user->name, name) == 0) {
            return node->user;
        }

        node = node->next;
    }

    return NULL;
}
