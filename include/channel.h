#ifndef CHANNEL_H
#define CHANNEL_H

#include "user.h"

typedef struct _Channel {
    int id;
    int max_users;
    char* name;
    char* topic;
    ListUsers* users;
} Channel;

typedef struct _NodeChannel {
    struct _NodeChannel* next;
    Channel* channel;
} NodeChannel;

typedef struct _ListChannels {
    NodeChannel* first;
    NodeChannel* last;
    unsigned int size;
} ListChannels;


Channel* new_channel(char* name, char* topic, int max_users);
ListChannels* new_list_channels();
NodeChannel* new_node_channel(Channel* channel);
void add_channel(ListChannels* list, Channel* channel);
int channel_contains_user(User* user, Channel* channel);
void broadcast_channels(User* user, char* message);
void channel_send_all(User* user, Channel* channel, char* message, unsigned short include_sender);
char* channel_get_description(Channel*);
void channel_leave(User* user, Channel* channel, char* message);
void channel_join(User* user, Channel* channel);
Channel* channel_get_by_name(char* channel_name);
void send_channel_topic(User* user, Channel* channel);
void send_channel_names(User* user, Channel* channel);
void channel_leave_all(User* user, char* message);
void channel_update_topic(User* user, Channel* channel, char* topic);

#endif
