#ifndef CONNECT_USER_H
#define CONNECT_USER_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

#include "user.h"
#include "util.h"
#include "messages.h"
#include "receive_commands.h"
#include "channel.h"

#define LISTENQ 10
#define MAXLINE 8192

char* server_hostname;
char* motd;
ListUsers* users;
ListChannels* channels;
pthread_mutex_t users_mutex;
pthread_mutex_t channels_mutex;

void connect_user(User *);
void handle_command(User*, char*, char**, char*);

#endif
